/*
 * ramdisk_test.h
 *
 * Reference: http://www.tldp.org/LDP/lkmpg/2.4/html/c577.htm
 *
 */

#include <stdio.h>
#include <string.h>
#include <time.h>
#include <fcntl.h>
#include <sys/ioctl.h>

/* IOCTL codes */
#define RD_CREATE  _IOW(0, 0, rddata_t)    /* int rd_creat(char *pathname) */
#define RD_MKDIR   _IOW(0, 1, rddata_t)    /* int rd_mkdir(char *pathname) */
#define RD_OPEN    _IOW(0, 2, rddata_t)    /* int rd_open(char *pathname) */
#define RD_CLOSE   _IOW(0, 3, rddata_t)    /* int rd_close(int fd) */
#define RD_READ    _IOW(0, 4, rddata_t)    /* int rd_read(int fd, char *address, int num_bytes) */
#define RD_WRITE   _IOW(0, 5, rddata_t)    /* int rd_write(int fd, char *address, int num_bytes) */
#define RD_LSEEK   _IOW(0, 6, rddata_t)    /* int rd_lseek(int fd, int offset) */
#define RD_UNLINK  _IOW(0, 7, rddata_t)    /* int rd_unlink(char *pathname) */
#define RD_READDIR _IOW(0, 8, rddata_t)    /* int rd_readdir(int fd, char *address) */

/* Ramdisk test data structure */
typedef struct
{
    /* Address for create/mkdir/open */
    char pathname[128];

    /* Address for write/read/readdir */
    char address[128];

    /* File descriptor for close/read/write/lseek/readdir */
    int fd;

    /* Number of bytes to read/write */
    unsigned int num_bytes;

    /* Position offset for lseek/ */
    unsigned int offset;

} rddata_t;
