ramdisk
=======

In this assignment you are going to implement your own filesystem and corresponding file operations on a RAM disk. You will need to use Linux to implement a kernel loadable module in this part of the assignment. However,  you will not have to modify the core kernel.

===============

Contributions:

Michelle Beard:
- User space fileio.h library
    - ioctl() calls to kernel module
    - rd_read/rd_write 
        - For large addresses, implemented a block by block read/write
- File operations in kernel module
    - rd_lseek
    - rd_open
    - rd_close
    - rd_readdir

Calvin Flegal:
- File operations in kernel module
    - rd_read
    - rd_write
- Bitmap indexing and manipulation
- File system initialization

John Ying:
- File operations in kernel module
    - rd_create
    - rd_mkdir
    - rd_unlink
- Indirect block indexing
- Initial setup of data structures
    - Block, superblock, inode, bitmap
    - Process list and file descriptors
