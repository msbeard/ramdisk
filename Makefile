obj-m += ramdisk_module.o

KVER := $(shell uname -r)
DIRS := $(shell pwd)

all:
	make -C /usr/src/linux-$(KVER) M=$(DIRS) modules
        
test:	ramdisk_test.c fileio.c
	gcc -o test ramdisk_test.c fileio.c

demo:	test_file.c fileio.c
	gcc -o demo -DUSE_RAMDISK test_file.c fileio.c
in:	
	insmod ramdisk_module.ko
	
rm:	
	rmmod ramdisk_module
	
clean: 
	make -C /usr/src/linux-$(KVER) M=$(DIRS) clean
	rm -f test demo