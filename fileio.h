/*
 * fileio.h
 *
 * Reference: http://www.tldp.org/LDP/lkmpg/2.4/html/c577.htm
 *
 */

#include <stdio.h>
#include <string.h>
#include <time.h>
#include <fcntl.h>
#include <sys/ioctl.h>

#define MAXSIZE 128 
#define CHUNKSIZE 256
#define DEBUG 0

/* IOCTL codes */
#define RD_CREATE  _IOW(0, 0, rddata_t)    /* int rd_creat(char *pathname) */
#define RD_MKDIR   _IOW(0, 1, rddata_t)    /* int rd_mkdir(char *pathname) */
#define RD_OPEN    _IOW(0, 2, rddata_t)    /* int rd_open(char *pathname) */
#define RD_CLOSE   _IOW(0, 3, rddata_t)    /* int rd_close(int fd) */
#define RD_READ    _IOW(0, 4, rddata_t)    /* int rd_read(int fd, char *address, int num_bytes) */
#define RD_WRITE   _IOW(0, 5, rddata_t)    /* int rd_write(int fd, char *address, int num_bytes) */
#define RD_LSEEK   _IOW(0, 6, rddata_t)    /* int rd_lseek(int fd, int offset) */
#define RD_UNLINK  _IOW(0, 7, rddata_t)    /* int rd_unlink(char *pathname) */
#define RD_READDIR _IOW(0, 8, rddata_t)    /* int rd_readdir(int fd, char *address) */

/* Ramdisk data structure */
typedef struct
{
    /* Address for create/mkdir/open */
    char pathname[MAXSIZE];

    /* Address for write/read/readdir */
    char address[256];

    /* File descriptor for close/read/write/lseek/readdir */
    int fd;

    /* Number of bytes to read/write */
    unsigned int num_bytes;

    /* Position offset for lseek/ */
    unsigned int offset;
    
    /* Operation status (set by the kernel module) */
    int res;
    
} rddata_t;

/**
 * Shortcut to create a ramdisk structure 
 * @param path Absolute path of file or directory
 * @param addr Address buffer
 * @param fd File descriptor 
 * @param bytes Number of bytes 
 * @param off Position offset 
 * @return Ramdisk data structure
 */
static rddata_t create_data(char* path, char* addr, int fd, unsigned int bytes,
                            unsigned int off);

/**
 * Create a regular file with absolute pathname from the
 * root of a directory tree
 * @param pathname Absolute path of file
 * @return 0 on success, -1 on failure
 */
int rd_create (char *pathname);

/**
 * Create a directory file with absolute pathname from the
 * root of the directory tree
 * @param pathname Absolute path of directory file
 * @return 0 on success, -1 on failure
 */
int rd_mkdir (char *pathname);

/**
 * Open an existing file corresponding to pathname 
 * @param pathname Absolute path of either a file or directory file 
 * @return Return either a file descriptor value on success or -1 on failure
 */
int rd_open (char *pathname);

/**
 * Close the corresponding file descriptor and release the file object 
 * @param fd File descriptor value
 * @return 0 on success, -1 on failure
 */
int rd_close (int fd);

/**
 * Read read up to num_bytes from a regular file identified by file descriptor,
 * fd, into a process' location at address. 
 * @param fd File descriptor of an open file
 * @param address Address of calling process
 * @param num_bytes Number of bytes to read from file
 * @return The actual number of bytes read, or -1 on failure
 */
int rd_read (int fd, char *address, int num_bytes);

/**
 * Write up to num_bytes from the specified address in the calling process to 
 * a regular file identified by file descriptor, fd. 
 * @param fd File descriptor
 * @param address Address of calling process
 * @param num_bytes Number of bytes to write
 * @return The actual number of bytes written, or -1 on failure
 */
int rd_write (int fd, char *address, int num_bytes);

/**
 * Set the file object's file position identified by file descriptor, fd, to 
 * offset, returning the new position, or the end of the file position if the 
 * offset is beyond the file's current size. 
 * @param fd File descriptor
 * @param offset Offset amount
 * @return Return new file position or 0 (EOF)
 */
int rd_lseek (int fd, int offset);

/**
 * Remove the filename with absolute pathname from the filesystem, freeing 
 * its memory in the ramdisk. 
 * @param pathname Absolute path to file or directory file
 * @return 0 on success, -1 on failure
 */
int rd_unlink (char *pathname);

/**
 * Read one entry from a directory file identified by fd and store the result 
 * in user-memory at the specified value of address. 
 * @param fd File descriptor 
 * @param address Address of calling process
 * @return 1 on success, -1 on failure
 * If directory is empty or last entry has been read by a previous call,
 * return 0. 
 */
int rd_readdir (int fd, char *address);