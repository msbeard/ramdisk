/*
 * ramdisk_test.c
 */

#include "fileio.h"

int main()
{
    int res, pfd, fd01, fd02, fd03, index_node_number;
    char *path = "/dir";
    char *f01 = "/dir/file.txt";
    char *f02 = "/dir/file01.txt";
    char *f03 = "/dir/foobar.txt";
    unsigned int bytes = 5000;
    char addr[bytes];
    char data[bytes];
    
    // Add some data
    memset (data, '1', sizeof (data));
    
    res = rd_mkdir (path);
    printf("res from mkdir was: %i\n", res);

    res = rd_create (f01);
    printf("res from create f01 was: %i\n", res);
    res = rd_create (f02);
    printf("res from create f02 was: %i\n", res);
    res = rd_create (f03);
    printf("res from create f03 was: %i\n", res);
    
    fd01 = rd_open (f01);
    printf("res from open f01 was: %i\n", fd01);
    
    res = rd_write (fd01, data, bytes);
    printf("res from write to f01 was: %i\n", res);
    
    res = rd_lseek (fd01, 0);
    printf("res from lseek to f01 was: %i\n", res);
    
    res = rd_read (fd01, addr, bytes);
    printf("res from read from f01 was: %i\n", res);
    printf("Print contents = %s\n", addr);
    
    res = rd_close (fd01);
    printf("res from close f01 was: %i\n", res);
  
    pfd = rd_open (path);
    printf("res from open path was: %i\n", pfd);

    memset (addr, 0, sizeof(addr));
    
    res = rd_readdir (pfd, addr);
    printf("res from readdir path was: %i\n", res);

    index_node_number = atoi(&addr[14]);
    printf ("Contents at addr: [%s,%d]\n", 
            addr, index_node_number);
    
    res = rd_close (pfd);
    printf("res from close path was: %i\n", res);

    res = rd_unlink (f03);
    printf("res from deletting f03 was: %i\n", res);
    
    res = rd_unlink (f02);
    printf("res from deletting f02 was: %i\n", res);
    
    res = rd_unlink (f01);
    printf("res from deletting f01 was: %i\n", res);
    
    return 0;
}
