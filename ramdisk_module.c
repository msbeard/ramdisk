/*
 * ramdisk.c
 *
 * References:
 * - http://www.tldp.org/LDP/lkmpg/2.4/html/c577.htm
 * - http://isis.poly.edu/kulesh/stuff/src/klist/
 *
 */

#include "ramdisk_module.h"

static void rd_create(rddata_t *rddata)
{
    rddata->res = createDirEntry(rddata->pathname, REG_TYPE);
}

static void rd_mkdir(rddata_t *rddata)
{
    rddata->res = createDirEntry(rddata->pathname, DIR_TYPE);
}

static void rd_open(rddata_t *rddata)
{
    process_t *proc;
    int i, size, parent;
    unsigned int node_num;
    const char *token[10];

    // Find inode corresponding to path
    size = parsePath(rddata->pathname, token);
    parent = locatePath(token, size, 0);
    
    printk("<1>rd_open: Parent = %d\n", parent);

    if (parent >= 0 && fileExistsInDir(token[size-1], parent))
    {
        printk("<1>rd_open: %s found!\n", token[size-1]);

        // Return file descriptor information
        // First need to lookup current process file descriptor table
        proc = getCurrentProcessInList ();
        if (proc != NULL)
        {
            // Find inode corresponding to filename
            lookupInodeByFilename (token[size-1], parent, &node_num);
            
            // Search for empty slot in processors 
            // file descriptor table and add file entry
            for (i = 0; i < FDNUM; i++)
            {
                if (proc->fd[i].init == 0)
                {
                    printk("<1>rd_open: Found empty slot %d in fd_t\n", i);
                    proc->fd[i].init = 1;  // initialized
                    proc->fd[i].position = 0;
                    proc->fd[i].inode = node_num;
                    rddata->fd = i;
                    rddata->res = SUCCESS;
                    printk("<1>rd_open: Successfully opened file!\n");
                    return;
                }
            }
            // All entries are full!
            printk("<1>rd_open: Unable to find space in fd table!\n");
            rddata->res = FAILURE;
            return;
        }
        else 
        {
            printk("<1>rd_open: Process not found!\n");
            rddata->res = FAILURE;
            return;
        } // proc not found!
    }
    else 
    {
        printk("<1>rd_open: File %s does not exist!\n", rddata->pathname);
        rddata->res = FAILURE;
        return;
    } // file does not exist!
}

static void rd_close(rddata_t *rddata)
{
    process_t *proc;

    // Return file descriptor information
    // First need to lookup current process file descriptor table
    proc = getCurrentProcessInList();
    if (proc != NULL) 
    {
        // Remove fd entry in proc fd_t
        printk("<1>rd_close: Removing entry %d\n", rddata->fd);
        proc->fd[rddata->fd].init = 0;
        proc->fd[rddata->fd].inode = 0;
        proc->fd[rddata->fd].position = 0;

        rddata->res = SUCCESS;
        return;
    } else 
    {
        printk("<1>rd_open: Process not found!\n");
        rddata->res = FAILURE;
        return;
    } 
}

static void rd_read(rddata_t *rddata)
{
    process_t *proc;
    block_t* currentBlock;
    fileobject_t thisFileObject;
    int bytesRead, thisFileiNodeIndex, j;
    bytesRead = 0;
    proc = getCurrentProcessInList ();
    if (proc != NULL ) 
    {
        // look in this proc fd table at the fd index
        thisFileObject = proc->fd[rddata->fd];
        if (thisFileObject.init == 0) 
        {
            printk("<1>rd_read: file descriptor object not initialized!\n");
            rddata->res = FAILURE;
            return;
        }
        thisFileiNodeIndex = thisFileObject.inode;
        if (inode_p[thisFileiNodeIndex].type == 'd') 
        {
            printk("rd_read: cannot write to directory file \n");
            rddata->res = FAILURE;
            return;
        }
        //compute the size
        printk("<1>rd_read: This file's inode_pointer points to inode index: %d\n",
               thisFileiNodeIndex);
        for (j=0; j < rddata->num_bytes; j++) 
        {
            if (j>=0 && j<BLOCKSIZE*8) 
            {
                //direct blocks.
                currentBlock = inode_p[thisFileiNodeIndex].location[j/BLOCKSIZE];
            }
            else if (j<64*BLOCKSIZE+8*BLOCKSIZE) 
            {
                //first indirect block
                currentBlock = inode_p[thisFileiNodeIndex].location[8];
                currentBlock = &currentBlock[(j/BLOCKSIZE)-8];
            }
            else if (j<inode_p[thisFileiNodeIndex].size) 
            {
                //second indirect, but not more than size
                currentBlock =  inode_p[thisFileiNodeIndex].location[9];
                currentBlock = &currentBlock[(j/BLOCKSIZE)-64-8];
            }

             //copy one to user, bytes read ++, position++
            printk("<1>rd_read: reading byte from location:  %u\n", 
                   (void*) currentBlock+thisFileObject.position);
            printk("<1>rd_read: byte read is: %c\n", 
                   *(currentBlock+thisFileObject.position));
            printk("<1>rd_read: filepos is: %u\n",
                   thisFileObject.position);
            memcpy((void*)((rddata->address)+bytesRead), 
                   (void*) currentBlock+thisFileObject.position, 1); 
            bytesRead += 1;
            thisFileObject.position += 1;
        }
        rddata->res = bytesRead;
        return;
    }
    else 
    {
        printk("<1>rd_read: Process not found!\n");
        rddata->res = FAILURE;
        return;
    }   
}

static void rd_write(rddata_t *rddata)
{
    ptrblock_t *ptrblock;
    block_t* blockWrite;
    process_t *proc;
    int thisFileiNodeIndex,j;
    block_t* firstBlockToWrite;
    int additionalBlocksNeeded, fullBlocksUsedByWrite,
        blocksAlreadyUsed, partialBlockByteMarker, index, bytesWritten;
    fileobject_t thisFileObject;
    bytesWritten = 0;
    // First need to lookup current process file descriptor table
    proc = getCurrentProcessInList ();
    if (proc != NULL)
    {
        printk("<1>rd_write: fd number is is: %u\n", rddata->fd);

        // look in this proc fd table at the fd index
        thisFileObject = proc->fd[rddata->fd];

        if (thisFileObject.init == 0) 
        {
            printk("<1>rd_write: file descriptor object not initialized!\n");
            rddata->res = FAILURE;
            return;
        }
        
        thisFileiNodeIndex = thisFileObject.inode;
        if (inode_p[thisFileiNodeIndex].type == 'd') 
        {
            printk("rd_write: cannot write to directory file \n");
            rddata->res = FAILURE;
            return;
        }
        
        printk("<1>rd_write: This file's inode_pointer points to inode index: %u\n",
               thisFileiNodeIndex);
        fullBlocksUsedByWrite = rddata->num_bytes/256;
        for (j=0; j < rddata->num_bytes; j++) 
        {
            partialBlockByteMarker = thisFileObject.position % 256;
            
            //calculate which location pointer to go to.
            index = inode_p[thisFileiNodeIndex].size / BLOCKSIZE;

            //either this block is full or file is empty, get next free data block
            if (inode_p[thisFileiNodeIndex].size % BLOCKSIZE == 0)
            {
                printk("<1>rd_write: Creating new data block\n");

                index = inode_p[thisFileiNodeIndex].size / BLOCKSIZE;

                // Direct pointer block
                if (index < 8)
                {
                    // Assign data block to pointer
                    inode_p[thisFileiNodeIndex].location[index] = getNextFreeBlock();
                }

                // Single-indirect pointer block
                else if (index < 72)     // 8 + 64
                {
                    // Assign pointer block to pointer
                    if (index == 8)
                        inode_p[thisFileiNodeIndex].location[8] = getNextFreeBlock();

                    ptrblock = (void *) inode_p[thisFileiNodeIndex].location[8];
                    index = index - 8;

                    // Assign data block to pointer within pointer block
                    ptrblock->location[index] = getNextFreeBlock();
                }

                // Double-indirect pointer block
                else if (index < 4168)   // 8 + 64 + 64*64
                {
                    // Assign first pointer block to pointer
                    if (index == 72)
                        inode_p[thisFileiNodeIndex].location[9] = getNextFreeBlock();

                    ptrblock = (void *) inode_p[thisFileiNodeIndex].location[9];
                    index = index - 8 - 64;

                    // Assign second pointer block to pointer within first pointer block
                    if (index % 64 == 0)
                        ptrblock->location[index/64] = getNextFreeBlock();

                    ptrblock = (void *) ptrblock->location[index/64];
                    index = index % 64;

                    // Assign data block to pointer within second pointer block
                    ptrblock->location[index] = getNextFreeBlock();
                }

               //write this block of data
               printk("<1>rd_write: Writing to new block!\n");
            }
            else 
            {
                // @todo the block is not full..

            }
            //in either case (new block or same block, write a byte)
          blockWrite = getCurrentBlock(thisFileiNodeIndex);
          if (blockWrite == NULL) 
          {
              printk("<1>rd_write: No additional blocks available!\n");
              rddata->res = bytesWritten;
              return;
          }
          //write one byte
          printk("<1>rd_write: copying byte to location: %u!\n",
                 (void*)blockWrite + thisFileObject.position);
          printk("<1>rd_write: copying byte from location: %u!\n",
                  (rddata->address)+bytesWritten);

          memcpy((void*)blockWrite + thisFileObject.position%256,
                  ((rddata->address)+bytesWritten), 1);

          printk("<1>rd_write:  byte  written is: %c\n",
                 *(blockWrite + thisFileObject.position));
          printk("<1>rd_write:  bytes  written is: %i\n", bytesWritten);
          printk("<1>rd_write:  rddata->address+ bytesWritten: %s\n", 
                 (rddata->address)+bytesWritten);

          //increase filesize
          inode_p[thisFileiNodeIndex].size +=1;
          
          //bump filepointer one byte
          thisFileObject.position +=1;
          bytesWritten += 1;
        } //end for loop
    }
    else 
    {
        printk("<1>rd_write: Process not found!\n");
        rddata->res = FAILURE;
        return;
    } // proc not found!
 
    rddata->res = bytesWritten;
}

static void rd_lseek(rddata_t *rddata)
{

    process_t *proc;
    int size, pos, offset, fd_i;
    inode_t this_inode;
    offset = rddata->offset;
    fd_i = rddata->fd;

    proc = getCurrentProcessInList ();
    if (proc != NULL)
    {        
        // Maximum position == file's current size
        pos = proc->fd[fd_i].position;
        this_inode = inode_p[proc->fd[fd_i].inode];
        size = this_inode.size;
        if (pos + offset > size)
        {
            // end of file position == current size
            proc->fd[fd_i].position = size;
            rddata->offset = size;
            rddata->res = SUCCESS;
            return;
        }
        else
        {
            proc->fd[fd_i].position = pos + offset;
            rddata->offset = pos + offset;
            rddata->res = SUCCESS;
            return;
        }
    }
    else
    {
        printk("<1>rd_lseek: Process not found!\n");
        rddata->res = FAILURE;
        return;
    }
}

static void rd_unlink(rddata_t *rddata)
{
    rddata->res = removeDirEntry(rddata->pathname, REG_TYPE);
}

static void rd_readdir(rddata_t *rddata)
{
    direntry_t *entry;
    int pos, fd_i, parent;
    process_t *proc;
    char dig[2];
    char fname[14];
    char result[17];
    
    fd_i = rddata->fd;
    proc = getCurrentProcessInList ();
    if (proc != NULL)
    {
        // Get current position
        if (proc->fd[fd_i].init == 0)
        {
            printk("<1>rd_readdir: Unable to read directory!\n");
            rddata->res = FAILURE;
            return;
        }
        
        pos = proc->fd[fd_i].position; // Should start at 0
        parent = proc->fd[fd_i].inode;
        printk("<1>rd_readdir: Position = %d\n", pos);
                
        printk("<1>rd_readdir: Inode = %d\n", parent);
        
        if (pos == inode_p[parent].size / sizeof (direntry_t))
        {
            // End of file
            // Deleted address buffer
            memset(rddata->address, 0, sizeof(rddata->address)+1);
            printk("<1>rd_readdir: End of file reached!\n");
            rddata->res = SUCCESS;
            return;
        }
        else
        {
            entry = getDirEntry(parent, pos);
            
            printk("<1>rd_readdir: File = %s\n", entry->filename);
            printk("<1>rd_readdir: Inode = %d\n", entry->inode);
            
            // Convert entry->inode to char
            sprintf(dig, "%d", entry->inode); // null terminated
            sprintf(fname, "%s", entry->filename); // null terminated
            
            if (DEBUG)
            {
                printk("<1>rd_readdir: Inode = %s : Sizeof = %d\n", 
                       dig, sizeof(dig));
                printk("<1>rd_readdir: Fname = %s : Sizeof = %d\n", 
                       fname, sizeof(fname));
            }
            
            memset (result, 0, sizeof(result)+1);
            memcpy (result, fname, sizeof(fname));
            memcpy (result+14, dig, sizeof(dig));

            if (DEBUG)
                printk("<1>rd_readdir: Result = %s : Sizeof = %d\n", 
                       result, sizeof(result));
                        
            memcpy(rddata->address, result, sizeof(result)+1);
            
            if (DEBUG)
                printk("<1>rd_readdir: Addr = %s\n", rddata->address+14);
            // Update position
            proc->fd[fd_i].position = pos+1;
            rddata->res = 1; // SUCCESS according to instructions on readdir
            return;
        } 
    }
    else 
    {
        // Proccess not found!
        printk("<1>rd_readdir: Process %d not found!\n", proc->id);
        rddata->res = FAILURE;
        return;
    }
}

static int createDirEntry(char *pathname, char type)
{
    direntry_t *entry;
    block_t *block;
    ptrblock_t *ptrblock;
    int size, parent, index;
    const char *token[10];

    // Find parent inode
    size = parsePath(pathname, token);
    parent = locatePath(token, size, 0);

    printk("<1>createDirEntry: Parent inode is %d\n", parent);

    // Error: path does not exist
    if (parent < 0)
    {
        printk("<1>createDirEntry: Path %s does not exist\n",
            pathname);
        return FAILURE;
    }

    // Error: file already exists
    if (fileExistsInDir(token[size-1], parent))
    {
        printk("<1>createDirEntry: File '%s' already exists\n",
            token[size-1]);
        return FAILURE;
    }

    // Get address of data block to write on
    block = getCurrentBlock(parent);
    printk("<1>createDirEntry: block = %u\n", (unsigned int) block);
    if (block == NULL)
    {
        printk("<1>createDirEntry: Maximum file size reached");
        return FAILURE;
    }

    // Assign directory entry
    entry = (void *) block + inode_p[parent].size % BLOCKSIZE;
    memcpy(entry->filename, token[size-1], strlen(token[size-1]) + 1);

    // Assign inode to entry
    entry->inode = getNextFreeInode(type);
    printk("<1>createDirEntry: Created inode %d for file '%s', type '%c'\n",
        entry->inode, token[size-1], type);

    // Update size in parent directory
    inode_p[parent].size += sizeof(direntry_t);

    // Current data block full, get next free data block
    if (inode_p[parent].size % BLOCKSIZE == 0)
    {
        printk("<1>createDirEntry: Creating new data block\n");

        index = inode_p[parent].size / BLOCKSIZE;
        
        // Direct pointer block
        if (index < 8)
        {
            // Assign data block to pointer
            inode_p[parent].location[index] = getNextFreeBlock();
        }

        // Single-indirect pointer block
        else if (index < 72)     // 8 + 64
        {
            // Assign pointer block to pointer
            if (index == 8)
                inode_p[parent].location[8] = getNextFreeBlock();

            ptrblock = (void *) inode_p[parent].location[8];
            index = index - 8;

            // Assign data block to pointer within pointer block
            ptrblock->location[index] = getNextFreeBlock();
        }

        // Double-indirect pointer block
        else if (index < 4168)   // 8 + 64 + 64*64
        {
            // Assign first pointer block to pointer
            if (index == 72)
                inode_p[parent].location[9] = getNextFreeBlock();

            ptrblock = (void *) inode_p[parent].location[9];
            index = index - 8 - 64;

            // Assign second pointer block to pointer within first pointer block
            if (index % 64 == 0)
                ptrblock->location[index/64] = getNextFreeBlock();

            ptrblock = (void *) ptrblock->location[index/64];
            index = index % 64;

            // Assign data block to pointer within second pointer block
            ptrblock->location[index] = getNextFreeBlock();
        }

        // todo: perform check on valid block addresses
    }

    return SUCCESS;
}

static int removeDirEntry(char *pathname, char type)
{
    direntry_t *entry;
    ptrblock_t *ptrblock;
    int i, parent;
    unsigned int size, node, blkcount, blkindex;
    const char *token[10];

    // Find parent inode
    size = parsePath(pathname, token);
    parent = locatePath(token, size, 0);

    printk("<1>removeDirEntry: Parent inode is %d\n", parent);

    // Error: path does not exist
    if (parent < 0)
    {
        printk("<1>removeDirEntry: Path %s does not exist\n",
            pathname);
        return FAILURE;
    }

    // Error: file does not exist
    if (!fileExistsInDir(token[size-1], parent))
    {
        printk("<1>removeDirEntry: File %s does not exist\n",
            token[size-1]);
        return FAILURE;
    }

    // Get inode of file to be removed
    lookupInodeByFilename(token[size-1], parent, &node);
    printk("<1>removeDirEntry: '%s' found at inode %u\n",
        token[size-1], node);

    // Error: file is root directory
    if (node == 0)
    {
        printk("<1>removeDirEntry: Cannot remove root directory\n");
        return FAILURE;
    }

    // Error: file is non-empty directory
    if (inode_p[node].type == DIR_TYPE && inode_p[node].size > 0)
    {
        printk("<1>removeDirEntry: Cannot remove '%s', is non-empty directory\n",
            token[size-1]);
        return FAILURE;
    }

    // Error: file is regular file that is currently open by a process
    // if (isFileOpenAtInode(node))
    // {
    //     printk("<1>removeDirEntry: Cannot remove '%s', is currently open by another process",
    //         token[size-1]);
    //     return FAILURE;
    // }

    // Error: file is not last directory entry in parent (TEMPORARY)
    entry = getDirEntry(parent, inode_p[parent].size / sizeof(direntry_t) - 1);
    if (strcmp(entry->filename, token[size-1]) != 0)
    {
        printk("<1>removeDirEntry: Cannot remove '%s', is not last directory entry ('%s')\n",
            token[size-1], entry->filename);
        return FAILURE;
    }
    // @todo: remove directory entry that is NOT the last one in parent

    // Count how many data blocks being used by file
    blkcount = inode_p[node].size / BLOCKSIZE + 1;
    printk("<1>removeDirEntry: Removing %d block(s)\n",
        blkcount);

    // Set data blocks to available
    for (i = 0; i < blkcount; i++)
    {
        if (i < 8)
        {
            blkindex = ((void *) inode_p[node].location[i] - (void *) base_p) / BLOCKSIZE;
        }
        else if (i < 8 + 64)
        {
            ptrblock = (void *) inode_p[node].location[8];

            if (i == 8)
            {
                blkindex = ((void *) ptrblock - (void *) base_p) / BLOCKSIZE;

                printk("<1>removeDirEntry: Removing block %u\n",
                    blkindex);

                setBitmapAtIndex(blkindex, 0);
                superblock_p->num_free_blocks++;
            }

            blkindex = ((void *) ptrblock->location[i-8] - (void *) base_p) / BLOCKSIZE;
        }
        else if (i < 8 + 64 + 64*64)
        {
            ptrblock = (void *) inode_p[node].location[9];

            if (i == 8 + 64)
            {
                blkindex = ((void *) ptrblock - (void *) base_p) / BLOCKSIZE;

                printk("<1>removeDirEntry: Removing block %u\n",
                    blkindex);

                setBitmapAtIndex(blkindex, 0);
                superblock_p->num_free_blocks++;
            }

            ptrblock = (void *) ptrblock->location[(i-8-64)/64];

            if ((i - 8 - 64) % 64 == 0)
            {
                blkindex = ((void *) ptrblock - (void *) base_p) / BLOCKSIZE;

                printk("<1>removeDirEntry: Removing block %u\n",
                    blkindex);

                setBitmapAtIndex(blkindex, 0);
                superblock_p->num_free_blocks++;
            }

            blkindex = ((void *) ptrblock->location[(i-8-64)%64] - (void *) base_p) / BLOCKSIZE;
        }

        printk("<1>removeDirEntry: Removing block %u\n",
            blkindex);

        setBitmapAtIndex(blkindex, 0);
        superblock_p->num_free_blocks++;
    }

    // Free up inode
    inode_p[node].type = NO_TYPE;
    inode_p[node].size = 0;
    superblock_p->num_free_inodes++;

    // Update parent directory's size
    inode_p[parent].size = inode_p[parent].size - sizeof(direntry_t);

    printk("<1>removeDirEntry: Completed removal of '%s'\n",
        token[size-1]);

    return SUCCESS;
}

static block_t* getCurrentBlock(unsigned int inode)
{
    ptrblock_t *block;
    unsigned int index = inode_p[inode].size / BLOCKSIZE;

    printk("<1>getCurrentBlock: index = %u\n", index);

    // Direct pointer
    if (index < 8)
        return inode_p[inode].location[index];

    // Single-indirect pointer
    if (index < 72)     // 8 + 64
    {
        // Single-indirect pointer block
        block = (void *) inode_p[inode].location[8];
        index = index - 8;

        return block->location[index];
    }

    // Double-indirect pointer
    if (index < 4168)   // 8 + 64 + 64*64
    {
        // Double-indirect pointer block
        block = (void *) inode_p[inode].location[9];
        index = index - 8 - 64;
        block = (void *) block->location[index/64];
        index = index % 64;

        return block->location[index];
    }

    // File size reached maximum
    return NULL;
}

static int getNextFreeInode(char type)
{
    block_t *block;
    int i;

    // Look in inode array for next available inode
    for (i = 0; i < (256 * BLOCKSIZE / INODESIZE); i++)
    {
        if (inode_p[i].type == NO_TYPE)
        {
            // Initialize inode
            inode_p[i].type = type;
            inode_p[i].size = 0;

            // Assign block
            if (!(block = getNextFreeBlock()))
                return -1;

            inode_p[i].location[0] = block;

            // Update superblock
            superblock_p->num_free_inodes--;

            // Available inode found
            return i;
        }
    }

    // No available inode found
    printk("<1>getNextFreeInode: No more available inodes");
    return -1;
}

static block_t* getNextFreeBlock()
{
    int i;

    // Look for next available block via bitmap
    for (i = 0; i < NUMBLOCKS; i++)
    {
        if (getBitmapAtIndex(i) == 0)
        {
            // Update bitmap
            setBitmapAtIndex(i, 1);

            // Update superblock
            superblock_p->num_free_blocks--;

            // Available block found
            return (void *) &base_p[i];
        }
    }

    // No available block found
    printk("<1>getNextFreeBlock: No more available blocks");
    return 0;
}

static void lookupInodeByFilename (const char *filename, int dir, 
                                   unsigned int *node)
{
    direntry_t *entry;
    int i;
    
    for (i = 0; i < inode_p[dir].size / sizeof(direntry_t); i++)
    {
        entry = getDirEntry(dir, i);
        if (entry == NULL)
            return;

        if (strcmp(entry->filename, filename) == 0)
        {
            *node = entry->inode;
            return;
        }
    }
}
 
static int locatePath(const char** tokens, int size, unsigned short node)
{
    direntry_t *dir;
    unsigned int i, index;

    // Path found
    if (size == 1)
        return node;

    // Loop through each directory entry
    for (i = 0; i < inode_p[node].size / sizeof(direntry_t); i++)
    {
        // Get which data block that i-th entry resides at
        index = i / sizeof(direntry_t);

        // Direct pointer block
        if (index < 8)
        {
            // Cast data block as array of directory entries
            dir = (void *) inode_p[node].location[index];
            printk("<1>locatePath: strcmp(%s, %s)\n",
                dir[i%16].filename, tokens[0]);

            // Matching directory entry to next folder
            if (!strcmp(dir[i%16].filename, tokens[0]))
                return locatePath(&tokens[1], size - 1, dir[i%16].inode);
        }

        // @todo: Check for indirect pointers
    }

    // Path not found
    return FAILURE;
}

static int parsePath (char* pathname, const char** token)
{    
    char *tok;
    char **bp;
    int indx;
    char *path = kmalloc (sizeof (char*)*128, GFP_KERNEL);
    memcpy(path, pathname, strlen(pathname)+1);
//    char fn[14];
    ++path; // remove first '/'
    bp = &path;
    indx = 0;

    while ((tok = strsep(bp, "/")))
    {
        if (DEBUG)
            printk("<1>parsePath: tok = %s\n", tok);
//        sprintf(fn, "%s", tok);
//        printk("<1>parsePath: fn = %s, size = %d\n", fn, sizeof(fn));
//        memcpy (token[indx], fn, sizeof(fn));
        token[indx] = tok;
        indx++;
    }

    kfree(path);

    return indx;

    // todo: trim tokens to 14 bytes each?
}

static int fileExistsInDir(const char *filename, int dir)
{
    direntry_t *entry;
    int i;

    for (i = 0; i < inode_p[dir].size / sizeof(direntry_t); i++)
    {
        entry = getDirEntry(dir, i);
        if (entry == NULL)
            return 0;

        if (strcmp(entry->filename, filename) == 0)
            return 1;
    }

    return 0;
}

static direntry_t* getDirEntry(unsigned int inode, unsigned int i)
{
    ptrblock_t *ptrblock;
    block_t *block;

    // Number of directory entries per block
    unsigned int size = sizeof(block_t) / sizeof(direntry_t);

    // Direct pointer block
    if (i < 8 * size)
    {
        block = inode_p[inode].location[i/size];
        i = i % size;

        return (void *) block + i * sizeof(direntry_t);
    }

    // Single-indirect pointer block
    if (i < (8 + 64) * size)
    {
        ptrblock = (void *) inode_p[inode].location[8];
        i = i - 8 * size;
        block = ptrblock->location[i/size];
        i = i % size;

        return (void *) block + i * sizeof(direntry_t);
    }

    // Double-indirect pointer block
    if (i < (8 + 64 + 64*64) * size)
    {
        ptrblock = (void *) inode_p[inode].location[9];
        i = i - (8 + 64) * size;
        ptrblock = (void *) ptrblock->location[i/(64*size)];
        i = i % (64 * size);
        block = ptrblock->location[i/size];
        i = i % size;

        return (void *) block + i * sizeof(direntry_t);
    }

    return NULL;
}

static void setBitmapAtIndex(int index, int value) 
{
    /* Make a byte to mask */
    unsigned char byte_value = 0x01;
    unsigned int bit_num = index % 8, byte_num;
    unsigned char *bitmap_base, *the_byte;

    byte_value = (byte_value << bit_num);
    bitmap_base = (unsigned char *) bitmap_p;

    /* Compute the byte offset and bit offset for index */
    byte_num = index / 8;
    the_byte = &(bitmap_base[byte_num]);

    if (value == 1)
    {
        *the_byte = *the_byte | byte_value;
    }
    else 
    {
        /* If you are zeroing you gotta be clever :) */
        byte_value = ~byte_value;
        *the_byte = *the_byte & byte_value;
    }
}

static unsigned int getBitmapAtIndex(unsigned int index)
{
    unsigned int bit_num = index % 8;
    unsigned char *bitmap_base = (unsigned char *) bitmap_p;
    unsigned int byte_num = index / 8;
    unsigned char the_byte = bitmap_base[byte_num];
    unsigned int the_bit = (the_byte >> bit_num) & 0x01;

    return the_bit;
}

static void printBitmap(int byteCount) 
{
    unsigned char *bitmap_base = (unsigned char *) bitmap_p;
    unsigned char this_byte;
    unsigned int this_bit;
    int i, j;

    // Loop over each byte in bitmap
    for (i = 0; i < byteCount; i++)
    {
        this_byte = bitmap_base[i];

        // iterate over bits
        for (j = 0; j < 8; j++)
        {
            this_bit = (this_byte >> j) & 0x01;
            printk("%i", this_bit);
        }

        printk("\n");
    }
}

static int isFileOpenAtInode(unsigned int node)
{
    process_t *process;
    int i;

    // Error: file is not a regular file
    if (inode_p[node].type != REG_TYPE)
        return 0;

    // Check for any matching inode in each process' fd table
    list_for_each_entry(process, &processList, list) 
    {
        for (i = 0; i < 16; i++)
        {
            if (process->fd[i].inode == node)
                return 1;
        }
    }

    return 0;
}

static process_t* getCurrentProcessInList()
{
    process_t *process;

    list_for_each_entry(process, &processList, list)
    {
        printk("<1>getCurrentProcessInList: pid = %u",
            process->id);

        if (process->id == current->pid) 
            return process;
    }

    return NULL;
}

static int ramdisk_ioctl(struct inode *inode, struct file *file, unsigned int cmd, unsigned long arg)
{
    rddata_t rddata;

    copy_from_user(&rddata, (rddata_t *) arg, sizeof(rddata_t));
    
    switch (cmd) 
    {
        case RD_CREATE:
            printk("<1>ramdisk: Call to RD_CREATE (%s)",
                rddata.pathname);
            rd_create(&rddata);
            copy_to_user((rddata_t*) arg, &rddata, sizeof(rddata_t));
            break;

        case RD_MKDIR:
            printk("<1>ramdisk: Call to RD_MKDIR (%s)",
                rddata.pathname);
            rd_mkdir(&rddata);
            copy_to_user((rddata_t*) arg, &rddata, sizeof(rddata_t));            
            break;

        case RD_OPEN:
            printk("<1>ramdisk: Call to RD_OPEN (%s)",
                rddata.pathname);
            rd_open(&rddata);
            copy_to_user((rddata_t*) arg, &rddata, sizeof(rddata_t));
            break;

        case RD_CLOSE:
            printk("<1>ramdisk: Call to RD_CLOSE (%d)",
                rddata.fd);
            rd_close(&rddata);
            copy_to_user((rddata_t*) arg, &rddata, sizeof(rddata_t));            
            break;

        case RD_READ:
            printk("<1>ramdisk: Call to RD_READ (%d, %d)",
                rddata.fd, rddata.num_bytes);
            rd_read(&rddata);
            copy_to_user((rddata_t*) arg, &rddata, sizeof(rddata_t));
            break;

        case RD_WRITE:
            printk("<1>ramdisk: Call to RD_WRITE (%d, %d)",
                rddata.fd, rddata.num_bytes);
            rd_write(&rddata);
            copy_to_user((rddata_t*) arg, &rddata, sizeof(rddata_t));            
            break;

        case RD_LSEEK:
            printk("<1>ramdisk: Call to RD_LSEEK (%d, %d)",
                rddata.fd, rddata.offset);
            rd_lseek(&rddata);
            copy_to_user((rddata_t*) arg, &rddata, sizeof(rddata_t));
            break;

        case RD_UNLINK:
            printk("<1>ramdisk: Call to RD_UNLINK (%s)",
                rddata.pathname);
            rd_unlink(&rddata);
            copy_to_user((rddata_t*) arg, &rddata, sizeof(rddata_t));
            break;

        case RD_READDIR:
            printk("<1>ramdisk: Call to RD_READDIR (%d)",
                rddata.fd);
            rd_readdir(&rddata);
            copy_to_user((rddata_t*) arg, &rddata, sizeof(rddata_t));
            break;

        default:
            printk("<1>ramdisk: IOCTL command %d not found!", cmd);
            return -EINVAL;
            break;
    }
    
    return 0;
}

static int ramdisk_open(struct inode *inode, struct file *file)
{
    process_t *process;
    int i;

    process = getCurrentProcessInList();

    // Current process exists in list
    if (process != NULL)
        return 0;

    // Allocate and save process ID
    process = kmalloc(sizeof(process_t), GFP_KERNEL);
    process->id = current->pid;

    // Add to process list
    INIT_LIST_HEAD(&process->list);
    list_add(&(process->list), &processList);

    // Allocate file descriptor table
    process->fd = kmalloc(sizeof(fileobject_t) * FDNUM, GFP_KERNEL);

    // Initialize table to zero for checks
    memset(process->fd, 0, sizeof(fileobject_t) * FDNUM);

    // All entries initialized to 0
    for (i = 0; i < FDNUM; i++)
        process->fd[i].init = 0;

    printk("<1>ramdisk_open: Added process ID %d to process list",
        current->pid);

    return 0;
}

static int ramdisk_release(struct inode *inode, struct file *file)
{
    process_t *process;
    list_head *pos, *q;

    // Remove process ID from process list
    list_for_each_safe(pos, q, &processList) 
    {
        process = list_entry(pos, process_t, list);

        if (process->id == current->pid) 
        {
            // Deallocate file descriptor table first
            kfree(process->fd);
            
            // Remove process
            list_del(pos);
            kfree(process);

            printk("<1>ramdisk_release: Removed process ID %d from process list", 
                current->pid);

            return 0;
        }
    }
    
    return 0;
}

static int __init initialization_routine(void)
{
    int i;

    printk("<1>ramdisk: Loading module");

    // Create proc entry
    proc_entry = create_proc_entry("ramdisk", 0444, NULL);
    
    if (!proc_entry) 
    {
        printk("<1>ramdisk: Error creating /proc entry");
        return 1;
    }

    // File operations
    ramdisk_proc_operations.ioctl   = ramdisk_ioctl;
    ramdisk_proc_operations.open    = ramdisk_open;
    ramdisk_proc_operations.release = ramdisk_release;

    proc_entry->proc_fops = &ramdisk_proc_operations;

    // Allocate partition
    base_p = vmalloc(MAXSIZE);
    if (base_p != NULL) 
    {
        // Fill partition with zeroes
        memset(base_p, 0, MAXSIZE);
        printk("<1>ramdisk: Allocated partition");
    }
    else 
    {
        printk("<1>ramdisk: Failed to allocate partition");
        return -1;
    }

    // Superblock
    superblock_p = (void *) base_p;
    superblock_p->num_free_blocks = NUMBLOCKS - (1 + 256 + BITMAPSIZE);
    superblock_p->num_free_inodes = 256 * BLOCKSIZE / INODESIZE;
    
    // Inodes
    inode_p = (void *) superblock_p + sizeof(block_t);
    for (i = 0; i < (256 * BLOCKSIZE / INODESIZE); i++)
        inode_p[i].type = NO_TYPE;

    // Bitmap
    bitmap_p = (void *) inode_p + sizeof(block_t) * 256;
    for (i = 0; i < (1 + BLOCKSIZE + BITMAPSIZE); i++) 
        setBitmapAtIndex(i, 1);

    // Data location
    data_p = (void *) bitmap_p + sizeof(block_t) * BITMAPSIZE;

    // Root
    if (getNextFreeInode(DIR_TYPE) < 0)
        printk("<1>ramdisk: No available inode for root");
    
    if (DEBUG)
    {
        printk("<1>superblock: free blocks:\t%u", superblock_p->num_free_blocks);
        printk("<1>superblock: free index nodes:\t%u", superblock_p->num_free_inodes);

        printk("<1>ramdisk: location of superblk:\t%u\t(%u)",
            (unsigned) superblock_p, (unsigned) superblock_p - (unsigned) base_p);
        printk("<1>ramdisk: location of inodes:\t%u\t(%u)",
            (unsigned) inode_p,      (unsigned) inode_p      - (unsigned) base_p);
        printk("<1>ramdisk: location of bitmap:\t%u\t(%u)",
            (unsigned) bitmap_p,     (unsigned) bitmap_p     - (unsigned) base_p);
        printk("<1>ramdisk: location of data:\t%u\t(%u)",
            (unsigned) data_p,       (unsigned) data_p       - (unsigned) base_p);

        printk("<1>ramdisk: sizeof block_t:\t%u",      sizeof(block_t));
        printk("<1>ramdisk: sizeof superblock_t:\t%u", sizeof(superblock_t));
        printk("<1>ramdisk: sizeof inode_t:\t%u",      sizeof(inode_t));
        printk("<1>ramdisk: sizeof bitmap_t:\t%u",     sizeof(bitmap_t));
    }
        
    return 0;
}

static void __exit cleanup_routine(void)
{
    printk("<1>ramdisk: Unloading ramdisk module");
    printk("<1>ramdisk: Freeing memory");

    vfree(base_p);
    remove_proc_entry("ramdisk", NULL);
}

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Michelle Beard, Calvin Flegal, John Ying");
module_init(initialization_routine);
module_exit(cleanup_routine);