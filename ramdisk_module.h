/*
 * ramdisk.h
 * 
 * References:
 * - http://www.tldp.org/LDP/lkmpg/2.4/html/c577.htm
 * - http://isis.poly.edu/kulesh/stuff/src/klist/
 *
 */

#include <linux/module.h>
#include <linux/init.h>
#include <linux/errno.h>        /* error codes */
#include <linux/proc_fs.h>
#include <asm/uaccess.h>
#include <linux/tty.h>          /* terminal */
#include <linux/sched.h>
#include <linux/irq.h>          /* IRQ handler */
#include <linux/interrupt.h>
#include <linux/vmalloc.h>
#include <linux/list.h>         /* list */
#include <asm/current.h>        /* current->pid */

#define DEBUG       1
#define SUCCESS     0
#define FAILURE     -1
#define MAXSIZE     2*1024*1024
#define BLOCKSIZE   256
#define INODESIZE   64
#define NUMBLOCKS   MAXSIZE / BLOCKSIZE
#define BITMAPSIZE  NUMBLOCKS / (8*256)
#define FDNUM       16          /* process holds at most 16 entries */
#define REG_TYPE   'r'
#define DIR_TYPE   'd'
#define NO_TYPE    'n'

/* IOCTL codes */
#define RD_CREATE  _IOW(0, 0, rddata_t)
#define RD_MKDIR   _IOW(0, 1, rddata_t)
#define RD_OPEN    _IOW(0, 2, rddata_t)
#define RD_CLOSE   _IOW(0, 3, rddata_t)
#define RD_READ    _IOW(0, 4, rddata_t)
#define RD_WRITE   _IOW(0, 5, rddata_t)
#define RD_LSEEK   _IOW(0, 6, rddata_t)
#define RD_UNLINK  _IOW(0, 7, rddata_t)
#define RD_READDIR _IOW(0, 8, rddata_t)

typedef char* string;
typedef unsigned char byte;
typedef struct list_head list_head;

/* Ramdisk test data structure */
typedef struct
{
    /* Address for create/mkdir/open */
    char pathname[128];

    /* Address for write/read/readdir */
    char address[256];
    
    /* File descriptor for close/read/write/lseek/readdir */
    int fd;

    /* Number of bytes to read/write */
    unsigned int num_bytes;

    /* Position offset for lseek */
    unsigned int offset;
    
    /* Operation status */
    int res;

} rddata_t;

/* Block structure (256 bytes) */
typedef struct
{
    /* Empty data */
    byte data[BLOCKSIZE];

} block_t;

/* Superblock structure (1 block = 256 bytes) */
typedef struct
{
    /* Number of available blocks */
    unsigned int num_free_blocks;

    /* Number of available inodes */
    unsigned int num_free_inodes;
    
    /* Padding to make sure the superblock uses a full block */
    byte padding[BLOCKSIZE - 
                 sizeof(unsigned int) - 
                 sizeof(unsigned int) - 
                 sizeof(block_t *)];

} superblock_t;

/* Index node structure (64 bytes) */
typedef struct
{
    /* Directory or regular file type ('d' or 'r' or 'n') */
    unsigned char type;

    /* File size in bytes */
    unsigned int size;

    /* Array of block pointers */
    block_t *location[10];

    /* Pad each inode out to 64 bytes including extra location pointer */
    byte padding[INODESIZE - 
                 sizeof(unsigned int) -
                 sizeof(unsigned char) - 
                 sizeof(block_t *) - 
                 sizeof(block_t *) * 10];

} inode_t;

/* Bitmap structure (4 blocks = 1024 bytes) */
typedef struct
{
    /* Keep track of free and allocated blocks in rest of partition */
    block_t data[BITMAPSIZE];

} bitmap_t;

/* Single-indirect pointer block structure */
typedef struct
{
    block_t *location[64];

} ptrblock_t;

/* Directory file entry structure (16 bytes) */
typedef struct
{
    /* File name */
    char filename[14];

    /* Corresponding inode */
    unsigned short inode;

} direntry_t;

/* File object structure */
typedef struct
{
    /* Current read/write file position */
    unsigned int position;

    /* Corresponding inode index */
    unsigned int inode;
    
    /* Initialized file object */
    int init;

} fileobject_t;

/* Process list entry structure */
typedef struct
{
    /* Process ID */
    unsigned int id;

    /* File descriptor table (assume 16 entries) */
    fileobject_t *fd;

    /* List */
    struct list_head list;

} process_t;

/* Process list */
LIST_HEAD(processList);

/* Partition pointers */
block_t *base_p;
superblock_t *superblock_p;
inode_t *inode_p;
bitmap_t *bitmap_p;
block_t *data_p;

/* Create a file */
static void rd_create(rddata_t *);

/* Create directory file */
static void rd_mkdir(rddata_t *);

/* Open a file */
static void rd_open(rddata_t *);

/* Close a file */
static void rd_close(rddata_t *);

/* Read a file */
static void rd_read(rddata_t *);

/* Write to a file */
static void rd_write(rddata_t *);

/* Move to specified offset */
static void rd_lseek(rddata_t *);

/* Remove file from filesystem */
static void rd_unlink(rddata_t *);

/* Read one entry from directory file and store result in user memory */
static void rd_readdir(rddata_t *);

/* Create new directory entry of specified type at path */
static int createDirEntry(char *, char);

/* Remove directory entry of specified type at path */
static int removeDirEntry(char *, char);

/* Return address of i-th directory entry in inode */
static direntry_t* getDirEntry(unsigned int, unsigned int);

/* Get address of current data block for inode */
static block_t* getCurrentBlock(unsigned int);

/* Get next available inode and return its index relative to root inode */
static int getNextFreeInode(char);

/* Get address of next available block */
static block_t* getNextFreeBlock(void);

/* Traverse path described by list of tokens */
static int locatePath(const char **, int, unsigned short);

/* Parse a pathname to get a list of files and size of the list */
static int parsePath(char*, const char **);

/* Check if filename exists in directory specified by inode */
static int fileExistsInDir(const char *, int);

/* Pass in your index as a BLOCK number, e.g., superblock = 0. Returns 0 or 1. */
static unsigned int getBitmapAtIndex(unsigned int);

/* Pass a one or zero into value */
static void setBitmapAtIndex(int, int);

/* Print bitmap first byteCount bytes */
static void printBitmap(int);

/* Search list to see if process is in list. If it is return 1 else return 0 */
static process_t* getCurrentProcessInList(void);

/* Find inode corresponding to a file */
static void lookupInodeByFilename(const char *, int, unsigned int *);

/* Check if file pointed by inode is currently open by another process */
static int isFileOpenAtInode(unsigned int);

/* Module file operations */
static struct proc_dir_entry *proc_entry;
static struct file_operations ramdisk_proc_operations;
static int ramdisk_ioctl(struct inode *, struct file *, unsigned int, unsigned long);
static int ramdisk_open(struct inode *, struct file *);
static int ramdisk_release(struct inode *, struct file *);
static int __init initialization_routine(void);
static void __exit cleanup_routine(void);