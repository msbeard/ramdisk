#include "fileio.h"

rddata_t create_data(char *path, char *addr, int fd, unsigned int bytes, 
                     unsigned int off)
{
    rddata_t rddata;
    memcpy(rddata.pathname, path, strlen(path) + 1);
    memcpy(rddata.address, addr, strlen(addr) + 1);
    
    rddata.fd = fd;
    rddata.num_bytes = bytes;
    rddata.offset = off;
    rddata.res = -1;
    return rddata;
}

int rd_create(char *pathname)
{
    if (DEBUG)
        printf("rd_create: Trying to create file %s\n", pathname);
    int PROC_FD = open("/proc/ramdisk", O_RDONLY);
    rddata_t rddata;
    rddata = create_data(pathname, "", 0, 0, 0);
    ioctl(PROC_FD, RD_CREATE, &rddata);
    if (DEBUG)
        printf("rd_create: Created [%s, %u]\n", pathname, rddata.res);
    return rddata.res;
}

int rd_mkdir(char *pathname)
{
    if (DEBUG)
        printf("rd_mkdir: Trying to create directory %s\n", pathname);
    int PROC_FD = open("/proc/ramdisk", O_RDONLY);
    rddata_t rddata;
    rddata = create_data(pathname, "", 0, 0, 0);
    ioctl(PROC_FD, RD_MKDIR, &rddata);
    if (DEBUG)
        printf("rd_mkdir: Created directory %s\n", pathname);
    return rddata.res;
}

int rd_open(char *pathname)
{
    if (DEBUG)
        printf("rd_open: Trying to open file %s\n", pathname);
    int PROC_FD = open("/proc/ramdisk", O_RDONLY);
    rddata_t rddata;
    rddata = create_data(pathname, "", 0, 0, 0);
    ioctl(PROC_FD, RD_OPEN, &rddata);
    if (DEBUG)
        printf("rd_open: Successfully opened file [%s, %d]\n", pathname, rddata.fd);
    return rddata.fd;
}

int rd_close(int fd)
{
    if (DEBUG)
        printf("rd_close: Closing fd[%d]\n", fd);
    int PROC_FD = open("/proc/ramdisk", O_RDONLY);
    rddata_t rddata;
    rddata = create_data("", "", fd, 0, 0);
    ioctl(PROC_FD, RD_CLOSE, &rddata);
    if (DEBUG)
        printf("rd_close: Closing!\n");
    return rddata.res;
}

int rd_read(int fd, char *address, int num_bytes)
{
    if (DEBUG)
        printf("rd_read: Trying to read [%d, %u]\n", fd, num_bytes);
    int off, i, rm;
    char buffer[CHUNKSIZE];
    int PROC_FD = open("/proc/ramdisk", O_RDONLY);
    rddata_t rddata;

    if (num_bytes <= CHUNKSIZE)
    {
        if (DEBUG) 
            printf("rd_read: Attempting to read %d bytes\n", num_bytes);
        rddata = create_data("", address, fd, num_bytes, 0);
        ioctl(PROC_FD, RD_READ, &rddata);
        memcpy (address, rddata.address, strlen(rddata.address));
        if (DEBUG) 
            printf("rd_read: Successfully read d\n", rddata.res);
        return rddata.res;
    }
    else
    {
        off = 0;
        rm = num_bytes;
        memset(address, 0, sizeof(address));  
        for (i = 0; i < num_bytes/CHUNKSIZE; i++)
        {
            if (DEBUG) 
                printf("rd_read: Attempting to read [%d, %d] bytes\n", off, i);
            memset(buffer, 0, CHUNKSIZE);
            rddata = create_data("", buffer, fd, CHUNKSIZE, 0);
            ioctl(PROC_FD, RD_READ, &rddata);
            if (DEBUG) 
                printf("rd_read: About to copy %d bytes to addr[%d]\n", 
                   CHUNKSIZE, off);
            // Partial copy
            memcpy (buffer, rddata.address, CHUNKSIZE);
            memcpy (address+off, buffer, sizeof(buffer)+1);
            off = off + CHUNKSIZE;
            rm = rm - CHUNKSIZE;
            if (DEBUG) 
                printf("rd_read: [%d] bytes left\n", rm);
        }
        if (rm > 0 && rm < CHUNKSIZE)
        {
            if (DEBUG) 
                printf("rd_read: Attempting to read final %d bytes\n", rm);
            // Read up to remaining bytes
            memset (buffer, 0, rm);
            rddata = create_data("", buffer, fd, rm, 0);
            ioctl(PROC_FD, RD_READ, &rddata);
            memcpy(address+off, rddata.address, rm+1);
        }
        if (DEBUG) 
            printf("rd_read: Successfully read %d bytes\n", strlen(address));
        return rddata.res;
    }
}

int rd_write(int fd, char *address, int num_bytes)
{
    int off, i, rm;
    char buffer[CHUNKSIZE];
    if (DEBUG) 
        printf("rd_write: Trying to write [%d, %u]\n", fd, num_bytes);
    int PROC_FD = open("/proc/ramdisk", O_RDONLY);
    rddata_t rddata;
    
    if (num_bytes <= CHUNKSIZE)
    {
        if (DEBUG) 
            printf("rd_write: Attempting to write %d] bytes\n", num_bytes);
        rddata = create_data("", address, fd, num_bytes, 0);
        ioctl(PROC_FD, RD_WRITE, &rddata);
        if (DEBUG) 
            printf("rd_write: Successfully written to file %d\n", rddata.fd);
        return rddata.res;
    }
    else
    {
        off = 0;
        rm = num_bytes;
        for (i = 0; i < num_bytes/CHUNKSIZE; i++)
        {
            if (DEBUG) 
                printf("rd_write: Attempting to write [%d, %d] bytes\n", off, i);
            // Copy CHUNKSIZE contents of address to buffer
            memset (buffer, 0, CHUNKSIZE);
            memcpy (buffer, address+off, CHUNKSIZE);
            rddata = create_data("", buffer, fd, CHUNKSIZE, 0);
            ioctl(PROC_FD, RD_WRITE, &rddata);
            off = off + CHUNKSIZE;
            rm = rm - CHUNKSIZE;
        }
        if (rm > 0 && rm < CHUNKSIZE)
        {
           if (DEBUG) 
               printf("rd_write: Attempting to write final %d bytes\n", rm);
           // Write remaining bytes
           memset (buffer, 0, rm);
           memcpy (buffer, address+off, rm);
           rddata = create_data("", buffer, fd, CHUNKSIZE, 0);
           ioctl(PROC_FD, RD_WRITE, &rddata);
        }
        if (DEBUG) 
            printf("rd_read: Successfully written %d\n", strlen(address));
        return rddata.res;        
    }
}

int rd_lseek(int fd, int offset)
{
    if (DEBUG) 
        printf("rd_lseek: Trying to seek at [%d, %d]\n", fd, offset);
    int PROC_FD = open("/proc/ramdisk", O_RDONLY);
    rddata_t rddata;
    rddata = create_data("", "", fd, 0, offset);
    ioctl(PROC_FD, RD_LSEEK, &rddata);
    if (DEBUG) 
        printf("rd_lseek: Successfully seeked to new offset[%d]\n", rddata.offset);
    return rddata.offset;
}

int rd_unlink(char *pathname)
{
    int PROC_FD = open("/proc/ramdisk", O_RDONLY);
    rddata_t rddata;
    rddata = create_data(pathname, "", 0, 0, 0);
    ioctl(PROC_FD, RD_UNLINK, &rddata);
    return rddata.res;
}

int rd_readdir(int fd, char *address)
{
    int PROC_FD = open("/proc/ramdisk", O_RDONLY);
    rddata_t rddata;
    rddata = create_data("", address, fd, 0, 0);
    ioctl(PROC_FD, RD_READDIR, &rddata);
    memcpy (address, rddata.address, sizeof(rddata.address)+1);
    return rddata.res;
}